import React from 'react';
import { css } from '@emotion/react';
import { Link } from 'gatsby';
import kebabCase from 'lodash/kebabCase';
import { colors } from './theme';

const ArticleTags = (props) => (
  <ul
    css={css`
      display: inline;
      list-style: none;
      margin: 0;
      padding: 0;
    `}
  >
    {props.tags.map((t, i) => (
      <li
        key={i}
        css={css`
          background: ${colors['gray-light']};
          color: ${colors['gray-dark']};
          border-radius: 4px;
          padding: 2px 0.6em;
          display: inline;
          margin-right: 8px;
        `}
      >
        <Link to={`tags/${kebabCase(t)}`}>{t}</Link>
      </li>
    ))}
  </ul>
);

export default ArticleTags;
