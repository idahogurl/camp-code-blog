import { get } from 'axios';

export default function getThreads() {
  const disqusApiKey =
    'SSE8WD0NRHxDiBQyUcczkf4IJZpnXrH0Nrzr49yIIzaXbbK83cLskJS2k4AXhvpm';
  const disqusAccessToken = 'ac30b535d9c249998b14214285738d66';
  const disqusShortname = 'campcode';

  const threads = get('https://disqus.com/api/3.0/threads/list.json', {
    params: {
      api_key: disqusApiKey,
      access_token: disqusAccessToken,
      forum: disqusShortname,
    },
  });

  return threads;
}
