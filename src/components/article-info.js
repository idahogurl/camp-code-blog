import React from 'react';
import { css } from '@emotion/react';
//import LogoAvatar from '../assets/logo-icon.svg';
import Byline from './byline';

export default function ArticleInfo({ date, readingTime, author }) {
  return (
    <div
      css={css`
        display: flex;
        margin-bottom: 24px;
      `}
    >
      {/* <LogoAvatar
        width="40"
        height="40"
        css={css`
          margin-right: 1em;
        `}
      /> */}
      <div>
        <Byline author={author} />
        <small
          css={css`
            display: block;
          `}
        >
          {date}
          &nbsp; &bull; &nbsp;
          {readingTime}
        </small>
      </div>
    </div>
  );
}
