import React, { Component } from 'react';
import { StaticQuery, graphql, Link } from 'gatsby';
import { css } from '@emotion/react';

import Logo from '../assets/logo.svg';
import { colors } from './theme';

class Header extends Component {
  render() {
    return (
      <StaticQuery
        query={graphql`
          query SearchIndexQuery {
            siteSearchIndex {
              index
            }
          }
        `}
        render={(data) => (
          <header
            css={css`
              position: sticky;
              z-index: 1;
              top: 0;
              width: 100%;
              background: ${colors.red};
              line-height: 0;
              padding: 10px 1rem .5rem 1rem;
              margin-bottom: -1px;
              display: flex;
            `}
          >
            <div
              css={css`
                display: flex;
                flex-wrap: wrap;
                align-items: center;
                justify-content: space-between;
                width: 100%;
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
              `}
            >
              <div
                css={css`
                  display: flex;
                  padding: 0.5rem 1rem;
                  flex-wrap: wrap;
                  align-items: center;
                `}
              >
                <Link
                  css={css`
                    box-shadow: none;
                    text-decoration: none;
                  `}
                  to="/"
                >
                  <Logo
                    css={css`
                        max-width: 100%;
                        height: auto;
                        display: block!important;
                        margin-right: 1rem!important;
                      `
                    }
                  />
                  
                </Link>
                

                    <nav>
                      <ul className="navbar-nav">
                        <li className="nav-item">
                          <a href="https://campcode.dev/index.html#top" className="nav-link"><i className="fas fa-user-circle icon" /> about</a>
                        </li>
                        <li className="nav-item">
                          <a href="https://campcode.dev/index.html" className="nav-link" id="nav-portfolio" aria-label="Go to portfolio"><i
                              className="fas fa-folder-open icon"></i> portfolio</a>
                        </li>
                        <li className="nav-item">
                          <i className="fas fa-book icon" /> blog
                        </li>
                        <li className="nav-item">
                          <a href="https://campcode.dev/index.html#contact" className="nav-link"><i className="fas fa-envelope icon" /> contact</a>
                        </li>
                      </ul>
                    </nav>
                    <br/>
                    {/* <span
                      css={css`
                        color: ${colors['gray-dark']};
                      `}
                    >
                      |
                    </span>
                    <a
                      href="/rss.xml"
                      css={css`
                        margin-left: 10px;
                      `}
                      className="nav-link"
                    >
                      RSS Feed
                    </a> */}
                  </div>
                  <div
                    css={css`
                      margin-top: 7px;
                    `}
                  >
                    {/* <Search searchIndex={data.siteSearchIndex.index} /> */}
                  </div>
                </div>
                {/* {!minimized && (
                  <div
                    css={css`
                      margin-top: 10px;
                    `}
                  >
                    <Bio />
                  </div>
                )} */}
          </header>
        )}
      />
    );
  }
}
export default Header;
