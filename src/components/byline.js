import React from 'react';

export default function Byline({ author }) {
  if (author[0] === undefined) {
    return null;
  }
  const byline = [];
  for (let index = 0; index < author.length; index++) {
    const authorItem = author[index];
    if (author.length > 1 && author.length - 1 === index) {
      byline.push(' and ');
    }

    byline.push(
      (authorItem.link && (
        <>
          <a href={authorItem.link} target="_blank" rel="noopener noreferrer">
            {authorItem.id}
          </a>
          {' '}
          {authorItem.title && `(${authorItem.title})`}
        </>
      )) || (
        <>
          {authorItem.id}
          {' '}
          {authorItem.title && `(${authorItem.title})`}
        </>
      ),
    );

    if (author.length > 2 && author.length - 1 !== index) {
      byline.push(', ');
    }
  }

  return byline.map((b, i) => <React.Fragment key={i}>{b}</React.Fragment>);
}
