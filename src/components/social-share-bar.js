import React from 'react';
import {
  FaFacebookF, FaTwitter, FaLinkedin, FaEnvelope,
} from 'react-icons/fa';
import { css } from '@emotion/react';
import ShareButton from './share-button';

const shareHrefs = {
  facebook: 'https://facebook.com/sharer/sharer.php',
  twitter: 'https://twitter.com/intent/tweet/',
  linkedIn: 'https://www.linkedin.com/shareArticle?mini=true',
  email: 'mailto:',
};
const ShareBar = (props) => (
  <ul
    css={css`
      display: flex;
      list-style: none;
      align-items: flex-end;
      padding: 0;
    `}
  >
    <li
      css={css`
        text-transform: uppercase;
        margin-right: 10px;
      `}
    >
      Share on
    </li>
    <li>
      <ShareButton
        href={`${shareHrefs.facebook}?u=${props.url}`}
        className="facebook"
      >
        <FaFacebookF size={28} />
      </ShareButton>
    </li>
    <li>
      <ShareButton
        href={`${shareHrefs.twitter}?text=${props.title}&url=${props.url}`}
        className="twitter"
      >
        <FaTwitter size={28} />
      </ShareButton>
    </li>
    <li>
      <ShareButton
        href={`${shareHrefs.linkedIn}&text=${props.title}&url=${props.url}`}
        className="linkedin"
      >
        <FaLinkedin size={28} />
      </ShareButton>
    </li>
    <li>
      <ShareButton
        href={`mailto:?subject=${props.title}&body=${props.url}`}
        target="_self"
        className="email"
      >
        <FaEnvelope size={28} />
      </ShareButton>
    </li>
  </ul>
);

export default ShareBar;
