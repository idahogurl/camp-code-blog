import React from 'react';
import { css } from '@emotion/react';

const ShareButton = (props) => (
  <a
    href={props.href}
    target={props.target || '_blank'}
    rel="noopener noreferrer"
  >
    <div
      css={css`
        width: 50px;
        height: 50px;
        display: flex;
        justify-content: center;
        align-items: center;
        border: 1px solid white;
      `}
      className={props.className}
    >
      {props.children}
    </div>
  </a>
);

export default ShareButton;
