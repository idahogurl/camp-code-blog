import React, { Component } from 'react';
import { Global, css } from '@emotion/react';
import { globalStyles, colors } from './theme';

import Header from './header';

class Layout extends Component {
  render() {
    const { children, minimized, style } = this.props;

    return (
      <>
        <Global styles={globalStyles} />
        <div>
          <Header />
          
          <div
            css={css`
              margin: 0 0.75em;
            `}
          >
            <main
              css={css`
                margin-left: auto;
                margin-right: auto;
                max-width: 40em;
                padding-top: ${minimized ? '20px' : '25px'};
                @media screen and (min-width: 768px) {
                  padding-top: ${minimized ? '25px' : '40px'};
                }
              `}
              style={style}
            >
              {children}
            </main>
          </div>
          <footer
            css={css`
              width: 100%;
              background: ${colors['gray-light']};
            `}
          >
            <div
              css={css`
                margin: 10px auto 0 auto;
                padding-top: 10px;
                padding-bottom: 10px;
                max-width: 40em;
                @media screen and (max-width: 590px) {
                  margin: 0.75em;
                }
              `}
            >
              <p>
              <small>Copyright &copy; Rebecca Vest and Camp Code Blog, {new Date().getFullYear()}. Unauthorized use and/or duplication of this material without express and written permission from this site&apos;s author and/or owner is strictly prohibited. Excerpts and links may be used, provided that full and clear credit is given to Rebecca Vest and the Camp Code Blog with appropriate and specific direction to the original content.
              Built with
              {' '}<a
                    href="https://www.gatsbyjs.org"
                    css={css`
                      text-decoration: underline;
                    `}
                  >
                    
                    Gatsby
                  </a>
                  .
                </small>
              </p>
            </div>
          </footer>
        </div>
      </>
    );
  }
}

export default Layout;
