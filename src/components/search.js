import React, { Component } from 'react';
import { Index } from 'elasticlunr';
import { css } from '@emotion/react';
import { Link } from 'gatsby';
import { debounce } from 'lodash';
import { FaSearch } from 'react-icons/fa';
import dayjs from 'dayjs';

import { colors } from './theme';

const hideListCss = css`
  display: none;
`;

export default class Search extends Component {
  constructor() {
    super();
    this.state = {
      results: [],
      showResults: false,
    };
  }

  inputRef = React.createRef();

  render() {
    const { showResults, results } = this.state;

    return (
      <div
        css={css`
          position: relative;
          display: flex;
          align-items: center;
          background-color: white;
          border: 1px solid ${colors.gray};
        `}
      >
        <input
          css={css`
            width: 200px;
            padding-left: 10px;
            height: 40px;
            position: relative;
            border: none;
          `}
          type="text"
          placeholder="Search titles or tags"
          onChange={this.handleSearch}
          onBlur={this.closeSearchResults}
          ref={this.inputRef}
        />
        <button
          type="button"
          css={css`
            width: 16px;
            height: 16px;
            padding: 0px;
            margin: 0 10px 0 10px;
            border: 0;
            background: transparent;
            cursor: pointer;
          `}
        >
          <FaSearch
            css={css`
              width: 16px;
              height: 16px;
              fill: ${colors.cyan};
            `}
          />
        </button>
        <ul
          css={[
            css`
              position: absolute;
              top: 28px;
              left: 0;
              background-color: white;
              margin-top: 3px;
              margin-left: 0;
              list-style: none;
              min-width: 275px;
              padding: 10px;
              border-radius: 3px;
              box-shadow: 0 1px 2px rgba(0, 0, 0, 0.25),
                0 0 1px rgba(0, 0, 0, 0.35);
            `,
            showResults ? '' : hideListCss,
          ]}
        >
          <li
            css={css`
              border-bottom: 1px solid ${colors['gray-light']};
              text-transform: uppercase;
            `}
          >
            Stories
          </li>
          {results.length === 0 && (
            <div
              css={css`
                color: ${colors['gray-dark']};
              `}
            >
              Sorry, no stories were found.
            </div>
          )}
          {results.map((page) => (
            <li key={page.id}>
              <Link to={page.path}>{page.title}</Link>
              <br />
              <small>{dayjs(page.date).format('MMMM DD, YYYY')}</small>
            </li>
          ))}
        </ul>
      </div>
    );
  }

  handleSearch() {
    this.search(this.inputRef.current.value);
    this.inputRef.current.focus();
  }

  handleSearch = this.handleSearch.bind(this);

  closeSearchResults() {
    this.setState({ showResults: false });
  }

  closeSearchResults = this.closeSearchResults.bind(this);

  getOrCreateIndex() {
    if (!this.index) {
      return Index.load(this.props.searchIndex);
    }
    return this.index;
  }

  getOrCreateIndex = this.getOrCreateIndex.bind(this);

  searchStories(query) {
    if (query.trim() === '') {
      this.setState({ showResults: false });
    } else {
      this.index = this.getOrCreateIndex();
      // Query the index with search string to get an [] of IDs
      const results = this.index
        .search(query, { expand: true })
        // Map over each ID and return the full document
        .map(({ ref }) => this.index.documentStore.getDoc(ref));

      this.setState({ showResults: true, results });
    }
  }

  searchStories = this.searchStories.bind(this);

  search = debounce(this.searchStories, 500);
}
