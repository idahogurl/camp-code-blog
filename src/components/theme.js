import { css } from '@emotion/react';

export const colors = {
  'text-black': '#231f20',
  'gray-dark': '#918f8f',
  gray: '#dcdbdb',
  'gray-light': '#f0efef',
  'gray-lightest': '#f7f7f7',

  red: '#8b0000',

  'cyan-dark': '#13737b',
  cyan: '#afeeee',
  'cyan-medium': '#81b9bf',
  'cyan-light': '#c0dcdf',
  'cyan-lighter': '#e0edef',
  'cyan-lightest': '#ebf3f4',

  'magenta-dark': '#9b1561',
  magenta: '#e0218e',
  'magenta-medium': '#e5b2cf',
  'magenta-light': '#f1d8e6',
  'magenta-lighter': '#faebf2',
  'magenta-lightest': '#fbf3f7',

  'tangerine-dark': '#aa311f',
  tangerine: '#f0533a',
  'tangerine-medium': '#ecafa7',
  'tangerine-light': '#f5d7d3',
  'tangerine-lighter': '#faebe9',
  'tangerine-lightest': '#fbf3f1',

  'gold-dark': '#b07e25',
  gold: '#e8c547',
  'gold-medium': '#e1be83',
  'gold-light': '#f0dec1',
  'gold-lighter': '#f7efe0',
  'gold-lightest': '#fbf5ed',

  facebook: '#1877f2',
  facebookHover: '#4267b2',
  twitter: '#1da1f2',
  twitterHover: '#006dbf',
  linkedIn: '#075AAE',
  linkedInHover: '#004182',
  email: '#e0218e',
  emailHover: '#9b1561',
};

export const globalStyles = css`
  
  * {
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
  }

  *,
  *:before,
  *:after {
    box-sizing: inherit;
  }

  :-moz-ui-invalid {
    box-shadow: none;
  }

  html {
    box-sizing: border-box;
    text-size-adjust: 100%;
    cursor: default;
    font-size: 16px;
    line-height: 26px;
    @media screen and (max-width: 590px) {
      font-size: 17px;
    }
  }

  body {
    font-family: 'Varela Round', -apple-system, system-ui, system-ui,
      'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;
    background-color: gray;
  }

  html,
  body {
    height: 100%;
    width: 100%;
    margin: 0;
  }

  h1 {
    font-weight: 700;
    font-size: 50px;
    line-height: 54px;
    margin-bottom: 30px;
    margin-top: 30px;
    @media screen and (max-width: 590px) {
      font-size: 34px;
      line-height: 38px;
      margin-bottom: 15px;
      margin-top: 25px;
    }
  }

  h2 {
    position: relative;
    clear: both;
    font-weight: bold;
    font-size: 38px;
    line-height: 42px;
    margin-bottom: 15px;
    margin-top: 45px;
    @media screen and (max-width: 590px) {
      font-size: 24px;
      line-height: 30px;
      margin-top: 30px;
    }
  }

  h3 {
    clear: both;
    font-weight: bold;
    font-size: 26px;
    margin-bottom: 20px;
    margin-top: 35px;
    @media screen and (max-width: 590px) {
      font-size: 20px;
      margin-bottom: 15px;
      margin-top: 30px;
    }
  }

  h4 {
    font-weight: bold;
    font-size: 16px;
    line-height: 26px;
    margin-bottom: 20px;
    margin-top: 25px;
    @media screen and (max-width: 590px) {
      font-size: 17px;
      margin-bottom: 15px;
      margin-top: 20px;
    }
  }

  li {
    line-height: 26px;
    margin-bottom: 8px;
  }

  p {
    margin-bottom: 25px;
    margin-top: 25px;
    line-height: 26px;
    @media screen and (max-width: 590px) {
      margin-bottom: 20px;
      margin-top: 20px;
    }
  }

  small {
    font-size: 14px;
    line-height: 20px;
  }

  hr {
    border: none;
    border-top: 1px solid gray;
    line-height: 0;
    margin-bottom: 20px;
    margin-top: 30px;
  }

  table {
    border-collapse: collapse;
    font-size: 16px;
    line-height: 24px;
  }

  a {
    color: ${colors['text-black']};
    text-decoration: none;
  }

  a:hover {
    color: ${colors['gray-dark']};
  }

  input {
    font-size: 14px;
  }

  blockquote {
    background-color: ${colors['gray-lightest']};
    line-height: 26px;
    margin: 30px 0;
    padding: 26px 30px;
    border-left-style: solid;
    border-left-width: 4px;
    border-color: ${colors.tangerine};
  }

  .facebook {
    background-color: ${colors.facebook};
    &:hover {
      background-color: ${colors.facebookHover};
    }
    svg {
      fill: white;
    }
  }

  .twitter {
    background-color: ${colors.twitter};
    &:hover {
      background-color: ${colors.twitterHover};
    }
    svg {
      fill: white;
    }
  }

  .linkedin {
    background-color: ${colors.linkedIn};
    &:hover {
      background-color: ${colors.linkedInHover};
    }
    svg {
      fill: white;
    }
  }

  .email {
    background-color: ${colors.email};
    &:hover {
      background-color: ${colors.emailHover};
    }
    svg {
      fill: white;
    }
  }

  .icon {
    padding-right: 10px;
  }

  .navbar-nav {
    margin: 0;
    display: flex;
    flex-wrap: wrap;
    list-style: none;
    flex-direction: column;
    @media screen and (min-width: 590.98px) {
      flex-direction: row;
      padding: 0;
    }
  }

  .nav-link {
    color: white;
    &:hover {
      color: ${colors['gray-dark']};
    }
  }

  .nav-item {
    color: #f5f5f5!important;
    padding-left: 7px!important;
    cursor: pointer;
    padding: 8px;
    margin: 0;
    line-height: 24px;
  }
  
  .nav-item:hover {
    background-color: #e8c547!important;
    color: #696969!important;
    cursor: pointer;
  }

  .header {
    margin-left: 10px;
    margin-right: 10px;
    text-transform: uppercase;
  }
`;
