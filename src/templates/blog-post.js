import React from 'react';
import { Link, graphql } from 'gatsby';
import { Disqus } from 'gatsby-plugin-disqus';
import { css } from '@emotion/react';

import Layout from '../components/layout';
import Seo from '../components/seo';
import ArticleInfo from '../components/article-info';
import SocialShareBar from '../components/social-share-bar';
import ArticleTags from '../components/article-tags';

class BlogPostTemplate extends React.Component {
  render() {
    const post = this.props.data.markdownRemark;
    const { siteUrl } = this.props.data.site.siteMetadata;
    const { previous, next } = this.props.pageContext;

    return (
      <Layout headerHeight="104" style={{backgroundColor: 'white', boxShadow: '0 0 2px 1px rgb(0 0 0 / 20%)', padding: '20px 10px 0 10px' }} minimized>
        <Seo
          title={post.frontmatter.title}
          description={post.frontmatter.description || post.excerpt}
          image={post.frontmatter.cover_image.childImageSharp.resize}
        />
        <h1
          css={css`
            margin-top: 0;
          `}
        >
          {post.frontmatter.title}
        </h1>
        <ArticleInfo
          readingTime={post.fields.readingTime.text}
          date={post.frontmatter.date}
          author={post.frontmatter.author}
        />
        <ArticleTags tags={post.frontmatter.tags} />
        <div
          css={css`
            a {
              text-decoration: underline;
            }
          `}
          dangerouslySetInnerHTML={{ __html: post.html }}
        />
        <hr />
        {(previous || next) && (
          <ul
            style={{
              display: 'flex',
              flexWrap: 'wrap',
              justifyContent: 'space-between',
              listStyle: 'none',
              padding: 0,
            }}
          >
            <li>
              {previous && (
                <Link to={previous.fields.slug} rel="prev">
                  ←
                  {' '}
                  {previous.frontmatter.title}
                </Link>
              )}
            </li>
            <li>
              {next && (
                <Link to={next.fields.slug} rel="next">
                  {next.frontmatter.title}
                  {' '}
                </Link>
              )}
            </li>
          </ul>
        )}
        <SocialShareBar
          url={`${siteUrl}${encodeURIComponent(post.fields.slug)}`}
          title={encodeURIComponent(post.frontmatter.title)}
        />
        <Disqus config={{
            /* Replace PAGE_URL with your post's canonical URL variable */
            url: siteUrl,
            /* Replace PAGE_IDENTIFIER with your page's unique identifier variable */
            identifier: post.id,
            /* Replace PAGE_TITLE with the title of the page */
            title: post.frontmatter.title,
        }}
    />
     
      </Layout>
    );
  }
}

export default BlogPostTemplate;

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        siteUrl
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      fields {
        slug
        readingTime {
          text
        }
      }
      frontmatter {
        title
        description
        date(formatString: "MMMM DD, YYYY")
        tags
        author {
          id
          title
          link
        }
        cover_image {
          publicURL
          childImageSharp {
            resize(width: 1200) {
              src
              height
              width
            }
          }
        }
      }
    }
  }
`;
