import React from 'react';
import PropTypes from 'prop-types';
import { css } from '@emotion/react';
// Components
import { Link, graphql } from 'gatsby';
import Layout from '../components/layout';
import Seo from '../components/seo';
import CircleArrow from '../assets/circle-arrow.svg';

// Utilities
import { colors } from '../components/theme';

const articleTitle = css`
  color: ${colors.red};
  font-size: 24px;
  line-height: 30px;
  font-size: 22px;
  @media screen and (max-width: 590px) {
    font-size: 22px;
    line-height: 28px;
  }
`;

const Tags = ({ pageContext, data }) => {
  const { tag } = pageContext;
  const { edges, totalCount } = data.allMarkdownRemark;
  const tagHeader = `${totalCount} post${
    totalCount === 1 ? '' : 's'
  } tagged with "${tag}"`;
  return (
    <Layout headerHeight="104" minimized>
      <Seo
        title={`'${tag}' Posts`}
        image={{
          src: '/hl_eng_blog_sharing.png',
          height: 732,
          width: 559,
        }}
        meta={[{ robots: 'noindex' }]}
      />
      <div
        css={css`
          margin-bottom: 100px;
          background-color: white;
          padding: 15px;
          padding-right: 15px;
        `}
      >
        <h1>{tagHeader}</h1>
        <ul
          css={css`
            list-style-type: none;
            padding-left: 0;
          `}
        >
          {edges.map(({ node }) => {
            const { slug } = node.fields;
            const { title, description, date } = node.frontmatter;
            return (
              <li key={slug}>
                <Link to={slug} css={articleTitle}>
                  {title}
                </Link>
                <div
                  css={css`
                    color: ${colors['gray-dark']};
                  `}
                >
                  {date}
                  ...
                  {description}
                </div>
              </li>
            );
          })}
        </ul>
        <Link
          to="/tags"
          css={css`
            :hover {
              fill: ${colors.gold};
            }
          `}
        >
          <span
            css={css`
              display: inline-block;
              vertical-align: top;
            `}
          >
            View all tags
          </span>
          {' '}
          <CircleArrow />
        </Link>
      </div>
    </Layout>
  );
};
Tags.propTypes = {
  pageContext: PropTypes.shape({
    tag: PropTypes.string.isRequired,
  }),
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      totalCount: PropTypes.number.isRequired,
      edges: PropTypes.arrayOf(
        PropTypes.shape({
          node: PropTypes.shape({
            frontmatter: PropTypes.shape({
              title: PropTypes.string.isRequired,
            }),
            fields: PropTypes.shape({
              slug: PropTypes.string.isRequired,
            }),
          }),
        }).isRequired,
      ),
    }),
  }),
};
export default Tags;
export const pageQuery = graphql`
  query($tag: String) {
    allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: [$tag] } } }
    ) {
      totalCount
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            title
            description
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
  }
`;
