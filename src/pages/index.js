import React from 'react';
import { Link, graphql } from 'gatsby';
import { css } from '@emotion/react';
import { keyBy } from 'lodash';
import { FaRegHeart, FaRegComment } from 'react-icons/fa';
import pluralize from 'pluralize';
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import Layout from '../components/layout';
import Seo from '../components/seo';
import ArticleInfo from '../components/article-info';
import ArticleTags from '../components/article-tags';
import getThreads from '../components/disqus-api';

import { colors } from '../components/theme';

const iconProps = {
  width: 25,
  height: 25,
  css: css`
    fill: ${colors['gray-dark']};
  `,
};

function IconWithLabel({ icon, label }) {
  return (
    <div
      css={css`
        display: flex;
        align-items: center;
      `}
    >
      {icon}
      <small
        css={css`
          margin-left: 10px;
          display: block;
        `}
      >
        {label}
      </small>
    </div>
  );
}

class BlogIndex extends React.Component {
  constructor() {
    super();
    this.state = {
      threads: undefined,
    };
  }

  componentDidMount() {
    getThreads()
      .then(({ data: { response } }) => {
        const threads = keyBy(response, 'link');
        this.setState({
          threads,
        });
      })
      .catch((e) => {
        // eslint-disable-next-line
        console.error(e);
      });
  }

  render() {
    const { data } = this.props;
    const { threads } = this.state;
    const posts = data.allMarkdownRemark.edges;
   
    return (
      <Layout>
        <Seo
          title="All posts"
          keywords={['healthline engineering', 'node', 'javascript', 'react']}
        />
        {posts.map(({ node }) => {
          const link = `${data.site.siteMetadata.siteUrl}${node.fields.slug}`;
          const coverImage = getImage(node.frontmatter.cover_image);
          
          let recommended = 0;
          let response = 0;
          if (threads && threads[link]) {
            recommended = threads[link].likes;
            response = threads[link].posts;
          }

          const title = node.frontmatter.title || node.fields.slug;
          return (
            <div
              key={node.fields.slug}
              css={css`
                padding: 25px;
                border-radius: 3px;
                background-color: white;
                box-shadow: 0 0 2px 1px rgb(0 0 0 / 20%);
                margin-bottom: 1.75em;
              `}
            >
              <ArticleInfo
                readingTime={node.fields.readingTime.text}
                date={node.frontmatter.date}
                author={node.frontmatter.author}
              />
              <h1
                css={css`
                  margin-top: 0;
                  margin-bottom: 30px;
                  font-size: 40px;
                  line-height: 44px;
                `}
              >
                <Link
                  css={css`
                    box-shadow: none;
                  `}
                  to={node.fields.slug}
                >
                  {title}
                </Link>
              </h1>
              <Link
                css={css`
                  box-shadow: none;
                `}
                to={node.fields.slug}
              >
                <GatsbyImage image={coverImage} alt="" />
              </Link>
              <p
                dangerouslySetInnerHTML={{
                  __html: node.frontmatter.description || node.excerpt,
                }}
              />
              <div
                css={css`
                  margin-bottom: 25px;
                `}
              >
                <ArticleTags tags={node.frontmatter.tags} />
              </div>
              <div
                css={css`
                  display: flex;
                  justify-content: space-between;
                `}
              >
                <IconWithLabel
                  icon={<FaRegHeart width={iconProps.width} height={iconProps.height} css={iconProps.css} />}
                  label={` ${recommended} ${pluralize(
                    'recommendation',
                    recommended,
                  )}`}
                />
                <IconWithLabel
                  icon={<FaRegComment width={iconProps.width} height={iconProps.height} css={iconProps.css} />}
                  label={` ${response} ${pluralize('response', response)}`}
                />
              </div>
            </div>
          );
        })}
      </Layout>
    );
  }
}

export default BlogIndex;

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        siteUrl
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt
          fields {
            slug
            readingTime {
              text
            }
          }
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            description
            tags
            author {
              id
              title
              link
            }
            cover_image {
              publicURL
              childImageSharp {
                gatsbyImageData(layout: CONSTRAINED)
              }
            }
          }
        }
      }
    }
  }
`;
