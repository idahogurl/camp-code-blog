import React from 'react';
import { StaticQuery, graphql } from 'gatsby';
import { css } from '@emotion/react';
import Layout from '../components/layout';
import Seo from '../components/seo';
import { colors } from '../components/theme';

class NotFoundPage extends React.Component {
  render() {
    const headerText = css`
      color: #c0dcdf;
      text-shadow: 3px 2px 0 ${colors.cyan};
      font-size: 6rem;
      text-align: center;
      padding-top: 20px;
      padding-bottom: 20px;
    `;
    return (
      <StaticQuery
        query={graphql`
          query {
            site {
              siteMetadata {
                title
              }
            }
          }
        `}
        render={(data) => (
          <Layout
            location={this.props.location}
            title={data.site.siteMetadata.title}
          >
            <Seo title="404: Not Found" />
            <h1 css={headerText}>404. Not Found</h1>
            <div
              css={css`
                max-width: 300px;
                width: 100%;
                margin: auto;
              `}
            >
              <img src="/404-tool-time.png" alt="Al from Tool Time Meme" />
            </div>
            <h2
              css={css`
                text-align: center;
                margin-bottom: 100px;
              `}
            >
              Sorry, Al says this page doesn&apos;t exist.
            </h2>
          </Layout>
        )}
      />
    );
  }
}

export default NotFoundPage;
