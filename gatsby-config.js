module.exports = {
  pathPrefix: '/camp-code-blog',
  siteMetadata: {
    title: 'CodeCamp: A blog by Rebecca Vest',
    author: 'Rebecca Vest',
    description:
      "Personal blog of Rebecca Vest",
    siteUrl: 'https://idahogurl.gitlab.io/camp-code-blog',
  },
  plugins: [
    "gatsby-transformer-yaml",
    "gatsby-plugin-emotion",
    "gatsby-plugin-image",
    "gatsby-plugin-react-helmet",
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          "gatsby-remark-reading-time",
          {
            resolve: "gatsby-remark-images",
            options: {
              maxWidth: 1024,
              wrapperStyle: () => "margin-left: 0; margin-right: 0;"
            }
          },
          {
            resolve: "gatsby-remark-responsive-iframe",
            options: {
              wrapperStyle: "margin-bottom: 1.0725rem"
            }
          },
          "gatsby-remark-prismjs",
          "gatsby-remark-copy-linked-files",
          "gatsby-remark-smartypants",
          "gatsby-remark-emoji",
          "gatsby-remark-external-links"
        ]
      }
    },
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "content",
        path: `${__dirname}/content/`
      },
      __key: "content"
    },
    {
      resolve: "@gatsby-contrib/gatsby-plugin-elasticlunr-search",
      options: {
        // Fields to index
        fields: ["title", "tags"],
        // How to resolve each field`s value for a supported node type
        resolvers: {
          // For any node of type MarkdownRemark, list how to resolve the fields` values
          MarkdownRemark: {
            title: node => node.frontmatter.title,
            tags: node => node.frontmatter.tags,
            date: node => node.frontmatter.date,
            path: node => node.fields.slug
          }
        }
      }
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /svg/,
        },
      },
    },
    {
      resolve: `gatsby-plugin-disqus`,
      options: {
          shortname: `campcode`
      }
  }
  ],
  mapping: {
    "MarkdownRemark.frontmatter.author": "AuthorYaml"
  }
};
